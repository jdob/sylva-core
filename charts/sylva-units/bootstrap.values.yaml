# Bootstrap values for sylva-units.
#
# These value are meant to be used as overrides to the default values from values.yaml
# when the chart is instantiated in the bootstrap cluster

unit_kustomization_spec_default:
  interval: 100000m   # for bootstrap, we don't care about having flux update things on the longterm
  retryInterval: 30s  # but we want it to retry quickly so that the system converges faster (quicker reaction when a dependency is met)

unit_helmrelease_spec_default:
  interval: 100000m

units:

  cluster:
    enabled: yes
    depends_on:
      reconcile-sylva-units: '{{ .Values.cluster.capi_providers.infra_provider | eq "capo" }}'

  calico:
    # overload calico definition in bootstrap context to deploy in the management cluster
    depends_on:
      cluster: true
    helmrelease_spec:
      kubeConfig:
        secretRef:
          name: '{{ .Values.cluster.name }}-kubeconfig'
    labels:
      suspend-on-pivot: "yes"  # this unit must be suspended before pivot

  management-cluster-flux:
    enabled: yes
    depends_on:
      cluster: true
      # FIXME; Flux wait/healthcheck on management cluster does not work as expected, it becomes ready as soon as manifests are applied
      # cluster readyness will actually be ensured by the retries on installations of this kustomization for now.
      # maybe cluster CRD is not compatible with kstatus? (see https://fluxcd.io/flux/units/kustomize/kustomization/#health-assessment)
      calico: '{{ .Values.cluster.capi_providers.bootstrap_provider | eq "cabpk" }}'
    repo: sylva-core
    kustomization_spec:
      path: ./kustomize-units/flux-system/base
      kubeConfig:
        secretRef:
          name: '{{ .Values.cluster.name }}-kubeconfig'
      targetNamespace: flux-system
      wait: true
      postBuild:
        substituteFrom:
        - kind: ConfigMap
          name: proxy-env-vars
    labels:
      suspend-on-pivot: "yes"  # this unit must be suspended before pivot

  management-cluster-configs:
    enabled: yes
    depends_on:
      management-cluster-flux: true
    repo: sylva-core
    kustomization_spec:
      # FIXME: This is very hacky, we should use an ad-hoc kustomisation instead of this job to re-create configmap and secrets on maangement cluster
      path: ./kustomize-units/kube-job
      wait: true
      force: true
      _patches:
      - target:
          kind: Job
          name: kube-job
        patch: |
          - op: replace
            path: /metadata/name
            value: copy-configs-job
      - target:
          kind: ConfigMap
          name: job-scripts
        patch: |
          - op: replace
            path: /data/kube-job.sh
            value: |
              #!/bin/bash
              set -e

              echo "-- Retrieve target cluster kubeconfig"
              kubectl get secret {{ .Values.cluster.name }}-kubeconfig -o jsonpath='{.data.value}' | base64 -d > management-cluster-kubeconfig

              echo "-- Copy secrets and configmaps from bootstrap to management cluster"
              kubectl get configmaps,secrets,gitrepository,helmrepository \
                -l copy-from-bootstrap-to-management= \
                -o json \
                  | jq '.items[] | del(.metadata.labels."helm.toolkit.fluxcd.io/name") | del(.metadata.labels."copy-from-bootstrap-to-management") | del(.metadata.labels."helm.toolkit.fluxcd.io/namespace") | del(.metadata.resourceVersion) | del(.metadata.uid) | del(.metadata.creationTimestamp)' \
                  | kubectl --kubeconfig management-cluster-kubeconfig apply -f -

              echo "-- All done"

  # instantiate 'sylva-units' chart again in the management cluster
  sylva-units:
    enabled: yes
    depends_on:
      management-cluster-configs: true
    repo: sylva-core
    labels:
      suspend-on-pivot: "yes"  # this unit must be suspended before pivot
    # Use a kustomization_spec alongside an helmrelease_spec,
    # this way we'll create the flux helmrelease into the management cluster
    kustomization_spec:
      kubeConfig:
        secretRef:
          name: '{{ .Values.cluster.name }}-kubeconfig'
    helmrelease_spec:
      releaseName: sylva-units
      chart:
        spec:
          chart: charts/sylva-units
          reconcileStrategy: Revision
          # copy sourceRef from bootstrap HelmRelease:
          sourceRef: '{{ lookup "helm.toolkit.fluxcd.io/v2beta1" "HelmRelease" "default" "sylva-units" | dig "spec" "chart" "spec" "sourceRef" dict | include "preserve-type" }}'
          # we copy the valuesFiles from the bootstrap HelmRelease, skipping
          # the bootstrap.values.yaml file (with or without the 'charts/sylva-units/'
          # prefix which is or isn't there depending on whether this is a deployment
          # relying on OCI artifacts)
          valuesFiles: |
            {{- without (lookup "helm.toolkit.fluxcd.io/v2beta1" "HelmRelease" "default" "sylva-units" | dig "spec" "chart" "spec" "valuesFiles" list) "bootstrap.values.yaml" "charts/sylva-units/bootstrap.values.yaml" | include "preserve-type" -}}
      interval: 1m0s
      # copy values and valuesFrom from current sylva-units HelmRelease
      # for now we keep the 'cluster' unit disabled, it produces CAPI resources
      # defining the management cluster, which we can't define before having done the pivot
      # (this value override is reverted after pivot by the pivot job below)
      values: |
        {{- $chartValues := lookup "helm.toolkit.fluxcd.io/v2beta1" "HelmRelease" "default" "sylva-units" | dig "spec" "values" dict -}}
        {{- $disableCluster := dict "units" (dict "cluster" (dict "enabled" false)) -}}
        {{- mergeOverwrite $chartValues $disableCluster | include "preserve-type" -}}
      valuesFrom: '{{ lookup "helm.toolkit.fluxcd.io/v2beta1" "HelmRelease" "default" "sylva-units" | dig "spec" "valuesFrom" list | include "preserve-type" }}'

  reconcile-sylva-units:
    enabled: '{{ .Values.cluster.capi_providers.infra_provider | eq "capo" }}'
    depends_on:
      capo-cluster-resources: true
    repo: sylva-core
    kustomization_spec:
      path: ./kustomize-units/kube-job
      wait: true
      force: true
      _patches:
      - target:
          kind: Job
          name: kube-job
        patch: |
          - op: replace
            path: /metadata/name
            value: reconcile-sylva-units-job
      - target:
          kind: ConfigMap
          name: job-scripts
        patch: |
          - op: replace
            path: /data/kube-job.sh
            value: |
              #!/bin/bash

              set -e

              reconcileAt=$(date -uIs)

              echo "-- Forcefully reconcile of the sylva-units HelmRelease"
              kubectl annotate --overwrite helmrelease/sylva-units reconcile.fluxcd.io/requestedAt="$reconcileAt"

              echo "-- Wait until the last release revision of the sylva-units HelmRelease has been incremented"
              kubectl wait --for=jsonpath='{.status.lastHandledReconcileAt}'=${reconcileAt} --timeout 180s helmrelease/sylva-units

              echo "-- Wait until the sylva-units HelmRelease is ready"
              kubectl wait --for=condition=ready --timeout 180s helmrelease/sylva-units

  pivot:
    enabled: yes
    depends_on:
      sylva-units: true
    repo: sylva-core
    kustomization_spec:
      path: ./kustomize-units/kube-job
      wait: true
      force: true
      _patches:
      - target:
          kind: Job
          name: kube-job
        patch: |
          - op: replace
            path: /metadata/name
            value: pivot-job
      - target:
          kind: ConfigMap
          name: job-scripts
        patch: |
          - op: replace
            path: /data/kube-job.sh
            value: |
              #!/bin/bash

              set -e

              echo "-- Signal that the pivot job has started. This is used in bootstrap.sh to prevent accidental re-runs"
              kubectl annotate --overwrite kustomizations.kustomize.toolkit.fluxcd.io cluster pivot/started=true

              echo "-- Retrieve target cluster kubeconfig"
              kubectl get secret {{ .Values.cluster.name }}-kubeconfig -o jsonpath='{.data.value}' | base64 -d > management-cluster-kubeconfig

              echo "-- Wait for cluster and machines to be ready as it is a required condition to move"
              kubectl wait --for condition=ControlPlaneReady --timeout 600s --all cluster
              kubectl wait --for condition=NodeHealthy --timeout 600s --all machine

              echo "-- Wait for all Kustomizations related to Cluster API to be installed on management cluster"
              # instead of looking at Ready condition (which temporarily become False if a reconciliation is ongoing)
              # we check that the observed generation is the same as the target
              for unit in capi {{ .Values.cluster.capi_providers.infra_provider }} {{ .Values.cluster.capi_providers.bootstrap_provider }}; do
                echo "------ ... waiting for $unit unit to be applied"
                generation=$(kubectl --kubeconfig management-cluster-kubeconfig get kustomizations.kustomize.toolkit.fluxcd.io capi -o jsonpath='{.metadata.generation}')
                kubectl --kubeconfig management-cluster-kubeconfig wait kustomization $unit \
                  "--for=jsonpath={status.observedGeneration}=$generation" \
                  --timeout=300s
              done

              echo "-- Suspend Kustomizations and HelmReleases in bootstrap cluster that relate to the management cluster"
              kubectl patch $(kubectl get helmreleases.helm.toolkit.fluxcd.io,kustomizations.kustomize.toolkit.fluxcd.io \
                -l suspend-on-pivot=yes -o name) --type=json --patch '[{"op": "replace", "path": "/spec/suspend", "value": true}]'

              kubectl patch helmreleases.helm.toolkit.fluxcd.io sylva-units --type=json \
                --patch '[{"op": "replace", "path": "/spec/suspend", "value": true}]'

              echo "-- Move cluster definitions from source to target cluster"
              clusterctl move --kubeconfig $KUBECONFIG --to-kubeconfig management-cluster-kubeconfig -v 3

              echo "-- Patch the sylva-units HelmRelease to allow the Flux cluster redefinition to happen in the management cluster"
              kubectl --kubeconfig management-cluster-kubeconfig \
                  patch helmrelease sylva-units --type=json --patch='[{"op":"remove","path":"/spec/values/units/cluster/enabled"}]'

              # NOTE: here we could add a reference to another valueFile that would deploy units specific to the management cluster
              # '[{"op":"add","path":"/spec/chart/spec/valuesFiles/-","value":"charts/sylva-units/management.values.yaml"}]'
              # and/or create a kustomisation on management-cluster to manage sylva-units helmRelease from git

              echo "-- Freeze reconciliation of current job Kustomisation in source cluster as we're done"
              kubectl annotate kustomizations pivot kustomize.toolkit.fluxcd.io/reconcile=disabled --overwrite

              echo "-- Accelerate reconciliation of the sylva-units HelmRelease"
              kubectl --kubeconfig management-cluster-kubeconfig \
                  annotate --overwrite helmrelease/sylva-units reconcile.fluxcd.io/requestedAt="$(date -uIs)"

              echo "-- Signal to bootstrap.sh that the pivot job has ended"
              kubectl annotate --overwrite kustomizations.kustomize.toolkit.fluxcd.io cluster pivot/started-

              echo "-- All done"


# only those components will be enabled on bootstrap cluster (this overrides 'units.<component>.enabled')
units_override_enabled:
  - cert-manager
  - capi
  - '{{ .Values.cluster.capi_providers.infra_provider }}'
  - '{{ .Values.cluster.capi_providers.bootstrap_provider }}'
  - '{{ tuple "metal3" (.Values.cluster.capi_providers.infra_provider | eq "capm3") | include "set-only-if" }}'
  - cluster
  - '{{ tuple "calico" (.Values.cluster.capi_providers.bootstrap_provider | eq "cabpk") | include "set-only-if" }}'
  - management-cluster-flux
  - management-cluster-configs
  - sylva-units
  - pivot
  - '{{ tuple "heat-operator" (.Values.cluster.capi_providers.infra_provider | eq "capo") | include "set-only-if" }}'
  - '{{ tuple "capo-cluster-resources" (.Values.cluster.capi_providers.infra_provider | eq "capo") | include "set-only-if" }}'
  - '{{ tuple "reconcile-sylva-units" (.Values.cluster.capi_providers.infra_provider | eq "capo") | include "set-only-if" }}'
