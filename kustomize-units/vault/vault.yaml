apiVersion: vault.banzaicloud.com/v1alpha1
kind: Vault
metadata:
  name: vault
spec:
  size: 1
  image: vault:1.13.2

  podAntiAffinity: kubernetes.io/hostname

  # Common annotations for all created resources
  annotations:
    common/annotation: "true"

  # Vault Pods , Services and TLS Secret annotations
  vaultAnnotations:
    type/instance: vault

  # Vault Configurer Pods and Services annotations
  vaultConfigurerAnnotations:
    type/instance: vaultconfigurer

  # Vault Pods , Services and TLS Secret labels
  vaultLabels:
    example.com/log-format: json

  # Vault Configurer Pods and Services labels
  vaultConfigurerLabels:
    example.com/log-format: string

  # Support for affinity Rules, same as in PodSpec
  # affinity:
  #   nodeAffinity:
  #     requiredDuringSchedulingIgnoredDuringExecution:
  #       nodeSelectorTerms:
  #       - matchExpressions:
  #         - key : "node-role.kubernetes.io/your_role"
  #           operator: In
  #           values: ["true"]

  # Support for pod nodeSelector rules to control which nodes can be chosen to run
  # the given pods
  # nodeSelector:
  #   "node-role.kubernetes.io/your_role": "true"

  # Support for node tolerations that work together with node taints to control
  # the pods that can like on a node
  # tolerations:
  # - effect: NoSchedule
  #   key: node-role.kubernetes.io/your_role
  #   operator: Equal
  #   value: "true"

  # Specify the ServiceAccount where the Vault Pod and the Bank-Vaults configurer/unsealer is running
  serviceAccount: vault

  # Specify the Service's type where the Vault Service is exposed
  # Please note that some Ingress controllers like https://github.com/kubernetes/ingress-gce
  # forces you to expose your Service on a NodePort
  serviceType: ClusterIP

  # Specify existing secret contains TLS certificate (accepted secret type: kubernetes.io/tls)
  # If it is set, generating certificate will be disabled
  # existingTlsSecretName: selfsigned-cert-tls

  # Specify threshold for renewing certificates. Valid time units are "ns", "us", "ms", "s", "m", "h".
  # tlsExpiryThreshold: 168h

  # Request an Ingress controller with the default configuration
  ingress:
    # Specify Ingress object annotations here, if TLS is enabled (which is by default)
    # the operator will add NGINX, Traefik and HAProxy Ingress compatible annotations
    # to support TLS backends
    annotations:
      kubernetes.io/ingress.class: nginx
      nginx.ingress.kubernetes.io/backend-protocol: HTTPS
    spec:
      rules:
      - host: ${VAULT_DNS}
        http:
          paths:
          - path: /
            pathType: Prefix
            backend:
              service:
                name: vault
                port:
                  number: 8200
      tls:
        - secretName: vault-tls
          hosts:
          - ${VAULT_DNS}


  volumeClaimTemplates:
    - metadata:
        name: vault-raft
      spec:
        # https://kubernetes.io/docs/concepts/storage/persistent-volumes/#class-1
        # storageClassName: ""
        accessModes:
          - ReadWriteOnce
        volumeMode: Filesystem
        resources:
          requests:
            storage: 1Gi

  volumeMounts:
    - name: vault-raft
      mountPath: /vault/file

  # Describe where you would like to store the Vault unseal keys and root token.
  unsealConfig:
    options:
      # The preFlightChecks flag enables unseal and root token storage tests
      # This is true by default
      preFlightChecks: true
      # The storeRootToken flag enables storing of root token in chosen storage
      # This is true by default
      storeRootToken: true
    kubernetes:
      secretNamespace: vault

  # A YAML representation of a final vault config file.
  # See https://www.vaultproject.io/docs/configuration/ for more information.
  config:
    storage:
      raft:
        path: "/vault/file"
    listener:
      tcp:
        address: "0.0.0.0:8200"
        tls_cert_file: /vault/tls/server.crt
        tls_key_file: /vault/tls/server.key
    api_addr: "https://vault:8200"
    cluster_addr: "https://vault:8201"
    ui: true

  statsdDisabled: true

  serviceRegistrationEnabled: true

  resources:
    # A YAML representation of resource ResourceRequirements for vault container
    # Detail can reference: https://kubernetes.io/docs/concepts/configuration/manage-compute-resources-container
    vault:
      limits:
        memory: 512Mi
        cpu: 200m
      requests:
        memory: 256Mi
        cpu: 100m

  # See: https://banzaicloud.com/docs/bank-vaults/cli-tool/#example-external-vault-configuration
  # The repository also contains a lot examples in the deploy/ and operator/deploy directories.
  externalConfig:
    policies:
      - name: allow_secrets
        rules: path "secret/*" {
          capabilities = ["create", "read", "update", "delete", "list"]
          }

    auth:
      - type: kubernetes
        roles:
          # Allow every pod in the vault namespace to use the secret kv store
          - name: default
            bound_service_account_names: ["default", "vault-secrets-webhook", "vault"]
            bound_service_account_namespaces: ["vault"]
            policies: ["allow_secrets"]
            ttl: 1h

    secrets:
      - path: secret
        type: kv
        description: Management Cluster Secrets
        options:
          version: 2

  vaultEnvsConfig:
    - name: VAULT_LOG_LEVEL
      value: debug
    - name: VAULT_CACERT
      value: /vault/tls/ca.crt



  # If you are using a custom certificate and are setting the hostname in a custom way
  # sidecarEnvsConfig:
  #   - name: VAULT_ADDR
  #     value: https://vault.local:8200

  # # https://kubernetes.io/docs/concepts/services-networking/add-entries-to-pod-etc-hosts-with-host-aliases/
  # vaultPodSpec:
  #   hostAliases:
  #   - ip: "127.0.0.1"
  #     hostnames:
  #     - "vault.local"

  # It is possible to override the Vault container directly:
  # vaultContainerSpec:
  #   lifecycle:
  #     postStart:
  #       exec:
  #         command:
  #              - setcap cap_ipc_lock=+ep /vault/plugins/orchestrate

  # Marks presence of Istio, which influences things like port namings
  istioEnabled: false

  existingTlsSecretName: vault-tls

  caNamespaces:
  - "*"
